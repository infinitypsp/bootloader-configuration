#ifndef INFINITYKINSTALLER_H_
#define INFINITYKINSTALLER_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
        INVALID_UPDATE,
        OFFICIAL_UPDATE,
        UNOFFICIAL_UPDATE
} UpdaterType;

unsigned int getBaryon(void);
int getTrueModel(void);


int mfcOpen(char *file);
int mfcOpenSection(char *name);
int mfcGetNextFile(char *file, int *size, int *cur, int *total, void *buf, int bufsize);
int mfcCloseSection();
int mfcClose();
int mfcInit(char *file);
int mfcAddDirectory(char *dir, int model);
int mfcAddFile(char *file, void *buf, int size, int model, int signcheck);
int mfcInitSection(char *name);
int mfcEndSection();
int mfcEnd();
int mfcLflashFatfmtStartFatfmt(int argc, void *argp);

UpdaterType verifyUpdate(const char *data, unsigned int size);

int mfcKernelLoadModuleBuffer(void *buffer, SceSize size, int flags, void *null);

#define MODEL(x) (1 << x)
#define ALL_MODELS (0xFFFFFFFF)

#ifdef __cplusplus
}
#endif

#endif // INFINITYKINSTALLER_H_
