TARGET = bootloader_config
OBJS = src/main.o src/timer.o src/infinityintro.o src/randomgenerator.o \
        src/blockallocator.o src/gltext.o src/fontgen.o \
		src/flashingrectangle.o src/buttonarbiter.o \
		libasm/libconfig_user.o \
		libasm/infinity_kinstaller.o \
		libasm/infinityBootloaderUpdater.o \
		libasm/scePower.o \
		src/menu.o \
		src/menuselectiontransition.o \
		src/menuselectionevent.o \
		src/updateinformation.o \
		src/updateinformationparser.o \
		src/internetupdatescreen.o \
		src/localupdatescreen.o \
		src/installupdatescreen.o \
		src/installupdate.o \
		src/mutex.o \
		src/mutexlocker.o \
		src/thread.o \
		src/functortransition.o \
		src/animations.o \
		src/psardrv.o \
		src/graphicsdevice.o src/transition.o src/buttontransition.o src/event.o src/buttonevent.o \
		src/homescreen.o \
		src/errorscreen.o \
		src/firework.o \
		src/configscreen.o \
		src/creditsscreen.o \
		src/compatibilitymodule.o \
		src/compatibilityconfig.o \
		src/viewmanager.o src/statemachine.o src/state.o src/view.o src/numberanimation.o src/texture.o src/tgatexture.o src/backgroundview.o src/fontmanager.o src/rectangle.o src/pageview.o src/font.o src/textrenderer.o src/application.o src/eventsource.o src/colouredrectangle.o src/particlesource.o src/updatescreen.o libasm/libinfinityUser.o

INCDIR = include $(BOOST_ROOT)
CFLAGS = -Os -G0 -Wall
CXXFLAGS = $(CFLAGS) -std=c++1y
ASFLAGS = $(CFLAGS) -c

LINK.c = $(LINK.cc)

LIBDIR = lib
LDFLAGS =
LIBS = -lstdc++ -lc -lintrafont -lpspvfpu -lpspgum -lpspgu -lpsprtc -lm -lpsphttp -lpspssl

BUILD_PRX = 1
#PSP_LARGE_MEMORY = 1
PSP_EBOOT_ICON = resources/icon0.png
EXTRA_TARGETS = EBOOT.PBP
PSP_EBOOT_TITLE = Infinity Bootloader Configuration

PSPSDK=$(shell psp-config --pspsdk-path)
include $(PSPSDK)/lib/build.mak
