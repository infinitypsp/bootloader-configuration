/*

Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.


 */

#include "updatescreen.h"
#include "homescreen.h"
#include "viewmanager.h"
#include "statemachine.h"
#include "numberanimation.h"
#include "buttontransition.h"
#include "tgatexture.h"
#include "backgroundview.h"
#include "pageview.h"
#include "fontmanager.h"
#include "textrenderer.h"
#include "font2.h"
#include "colouredrectangle.h"
#include "particlesource.h"
#include "intraFont.h"
#include "errorscreen.h"
#include "configscreen.h"

#include <libinfinity.h>
#include <pspsdk.h>
#include <pspkernel.h>
#include <pspctrl.h>

#include "vertextype.h"
#include <pspgu.h>
#include <pspgum.h>

#include <string>
#include <iostream>
#include <sstream>

PSP_MODULE_INFO("bootloader_config", 0, 1, 0);
PSP_HEAP_SIZE_KB(-4096);
PSP_MAIN_THREAD_ATTR(THREAD_ATTR_USER);

namespace {
int exit_callback (int arg1, int arg2, void *common)
{
    sceKernelExitGame();
    return 0;
}

int update_thread (SceSize args, void *argp)
{
    int cbid = sceKernelCreateCallback("Exit Callback", exit_callback, NULL);
    sceKernelRegisterExitCallback(cbid);
    sceKernelSleepThreadCB();
    return 0;
}


void setup_callbacks (void)
{
    int id;

    if ((id = sceKernelCreateThread("update_thread", update_thread, 0x11, 0xFA0, 0, 0)) >= 0)
        sceKernelStartThread(id, 0, 0);
}

void addSmoothParallaxTransitionLeft(auto source, auto dest, auto parallax)
{
    auto transition = new ButtonTransition(PSP_CTRL_LEFT);
    auto animation = std::make_shared<NumberAnimation>(0, 480);

    auto easing = [](float t, float b, float c, float d)
    {
        t /= d;
        return -c * t*(t-2) + b;
    };

    animation->setHandler([source, dest, easing](float number)
    {
        auto newval = easing(number, 0.f, 480.f, 480.f);

        ScePspFVector3 moveOut = { newval, 0, 0 };
        ScePspFVector3 moveIn = { -(480 - newval), 0, 0 };

        auto model = source->model();
        gumLoadIdentity(model);
        gumTranslate(model, &moveOut);

        model = dest->model();
        gumLoadIdentity(model);
        gumTranslate(model, &moveIn);
    });

    auto parallaxAnimation = std::make_shared<NumberAnimation>(0, 100);

    parallaxAnimation->setHandler([parallax, easing](float number)
    {
        auto newval = easing(number, 0.f, 100.f, 100.f);

        ScePspFVector3 moveOut = { newval, 0, 0 };
        auto model = parallax->model();
        gumLoadMatrix(model, parallax->loadModel());
        gumTranslate(model, &moveOut);
    });

    parallaxAnimation->onComplete([parallax]()
    {
        ScePspFVector3 moveOut = { 100, 0, 0 };
        auto model = parallax->model();
        gumLoadMatrix(model, parallax->loadModel());
        gumTranslate(model, &moveOut);
        parallax->saveModel(model);
        return false;
    });

    transition->addAnimation(animation);
    transition->addAnimation(parallaxAnimation);
    source->addTransition(transition, dest);
    source->setPageLeft(true);
}

void addSmoothParallaxTransitionRight(auto source, auto dest, auto parallax)
{
    auto transition = new ButtonTransition(PSP_CTRL_RIGHT);
    auto animation = std::make_shared<NumberAnimation>(0, 480);

    auto easing = [](float t, float b, float c, float d)
    {
        t /= d;
        return -c * t*(t-2) + b;
    };

    animation->setHandler([source, dest, easing](float number)
    {
        auto newval = easing(number, 0.f, 480.f, 480.f);

        ScePspFVector3 moveOut = { -newval, 0, 0 };
        ScePspFVector3 moveIn = { (480 - newval), 0, 0 };

        auto model = source->model();
        gumLoadIdentity(model);
        gumTranslate(model, &moveOut);

        model = dest->model();
        gumLoadIdentity(model);
        gumTranslate(model, &moveIn);
    });

    auto parallaxAnimation = std::make_shared<NumberAnimation>(0, 100);

    parallaxAnimation->setHandler([parallax, easing](float number)
    {
        auto newval = easing(number, 0.f, 100.f, 100.f);

        ScePspFVector3 moveOut = { -newval, 0, 0 };
        auto model = parallax->model();
        gumLoadMatrix(model, parallax->loadModel());
        gumTranslate(model, &moveOut);
    });

    parallaxAnimation->onComplete([parallax]()
    {
        ScePspFVector3 moveOut = { -100, 0, 0 };
        auto model = parallax->model();
        gumLoadMatrix(model, parallax->loadModel());
        gumTranslate(model, &moveOut);
        parallax->saveModel(model);
        return false;
    });

    transition->addAnimation(animation);
    transition->addAnimation(parallaxAnimation);
    source->addTransition(transition, dest);
    source->setPageRight(true);
}

class TestScreen : public PageView
{
public:
    TestScreen(ViewManager *viewManager, unsigned int color)
        : PageView(viewManager, "Test Screen")
        , m_opacity(1.f)
        , m_latinText(new TextRenderer(new Font("flash0:/font/ltn0.pgf")))
    {
        m_color = color;
        m_latinText->font()->setColour(Font::WHITE);

        std::stringstream ss;
        ss << "Infinity Internal Version: R" << std::hex << infGetVersion() << " CORE: R" << infGetCoreVersion();
        m_version = ss.str();
    }

    virtual void render(void)
    {
    	sceGuDisable(GU_TEXTURE_2D);

        Vertex *triangle = static_cast<Vertex*>(sceGuGetMemory(sizeof(Vertex)*2));

        triangle[0].colour = m_color;
        triangle[0].x = 240-20;
        triangle[0].y = 136-20.f;
        triangle[0].z = 0.0f;
        triangle[1].colour = m_color;
        triangle[1].x = 240+20.f;
        triangle[1].y = 136+20.f;
        triangle[1].z = 0.0f;

        sceGumDrawArray(GU_SPRITES, GU_COLOR_8888 | GU_VERTEX_32BITF | GU_TRANSFORM_3D, 2, 0, triangle);

        PageView::render();
    }

    virtual void update(float t)
    {
        PageView::update(t);
    }

    virtual void setOpacity(float opacity) override
    {
        m_opacity = opacity;
        int alpha = 0xFF * opacity;

        if (alpha > 0xFF) alpha = 0xFF;
        if (alpha < 0) alpha = 0;

        m_color &= 0xFFFFFF;
        m_color |= alpha << 24;
        m_latinText->font()->setOpacity(opacity);
    }

    float opacity(void) override
    {
        return m_opacity;
    }

private:
    unsigned int m_color;
    float m_opacity;
    TextRenderer *m_latinText;
    std::string m_version;
};
}

#include "infinityintro.h"
#include "creditsscreen.h"
#include "animations.h"

int main(int argc, char *argv[])
{
    setup_callbacks();
    intraFontInit();
    ViewManager *manager = new ViewManager();

    // load libconfig
    SceUID modid = sceKernelLoadModule("libconfig.prx", 0, NULL);

    if (modid < 0)
    {
        manager->addView(new ErrorScreen("Could not load libconfig.prx", modid));
        manager->exec();
        return 0;
    }

    int res = sceKernelStartModule(modid, 0, NULL, NULL, NULL);

    if (res < 0)
    {
        manager->addView(new ErrorScreen("Could not start libconfig.prx", res));
        manager->exec();
        return 0;
    }

    // load kinstaller
    modid = sceKernelLoadModule("infinity_kinstaller.prx", 0, NULL);

    if (modid < 0)
    {
        manager->addView(new ErrorScreen("Could not load infinity_kinstaller.prx", modid));
        manager->exec();
        return 0;
    }

    res = sceKernelStartModule(modid, 0, NULL, NULL, NULL);

    if (res < 0)
    {
        manager->addView(new ErrorScreen("Could not start infinity_kinstaller.prx", res));
        manager->exec();
        return 0;
    }

    auto homeScreen = std::make_shared<HomeScreen>(manager);
    auto configScreen = std::make_shared<ConfigScreen>(manager);
    auto updateScreen = std::make_shared<UpdateScreen>(manager);
    auto parallax = new BackgroundView(manager);
    auto creditsScreen = std::make_shared<CreditsScreen>(manager, parallax);

    parallax->setTextureLeft(new TgaTexture("parallaxleft.tga"));
    parallax->setTextureRight(new TgaTexture("parallaxright.tga"));

    manager->addView(parallax);

    // add smooth transitions
    addSmoothParallaxTransitionLeft(homeScreen.get(), configScreen.get(), parallax);
    addSmoothParallaxTransitionRight(configScreen.get(), homeScreen.get(), parallax);
    addSmoothParallaxTransitionLeft(updateScreen.get(), homeScreen.get(), parallax);
    addSmoothParallaxTransitionRight(homeScreen.get(), updateScreen.get(), parallax);
    ButtonTransition *btn = new ButtonTransition(PSP_CTRL_SELECT);
    smoothFadeOutAnimation(homeScreen.get(), creditsScreen.get(), btn);
    ButtonTransition *rt = new ButtonTransition(PSP_CTRL_RTRIGGER);
    smoothFadeOutAnimation(creditsScreen.get(), homeScreen.get(), rt);

    // add state machine
    StateMachine machine;
    machine.addState(homeScreen);
    machine.addState(configScreen);
    machine.addState(updateScreen);
    machine.setInitialState(homeScreen);
    machine.start();

    manager->setStateMachine(&machine);
    manager->exec();
    return 0;
}
