/*

Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.


 */

#include "configurationmenu.h"
#include "buttonarbiter.h"
#include "initstate.h"
#include "scene.h"

#include <config.h>

#include <utility>

#include <stdio.h>

ConfigurationMenu::ConfigurationMenu(void)
    : m_buttonArbiter(new ButtonArbiter())
    , m_timer(Timer::instance())
{
    printf("creating init\n");
    m_state = std::make_shared<InitState>(this);
}

ConfigurationMenu::~ConfigurationMenu(void)
{

}

void ConfigurationMenu::setState(StatePtr newState)
{
    m_state->end();
    std::swap(m_state, newState);
    m_buttonArbiter->reset();
    m_state->begin(newState);
}

ButtonArbiter *ConfigurationMenu::buttonArbiter(void)
{
    return m_buttonArbiter;
}

void ConfigurationMenu::begin(void)
{
    m_timer->start();
    m_state->begin(nullptr);

    loop();
}

void ConfigurationMenu::loop(void)
{
    while (1)
    {
        if (m_state->scene() == nullptr)
        {
            sceKernelDelayThread(5*1000);
            continue;
        }

        //m_buttonArbiter->update();

        m_state->scene()->update(m_timer->elapsed());
        m_state->scene()->render();
    }
}
