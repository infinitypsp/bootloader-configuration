/*

Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.


 */

#include "errorstate.h"
#include "gltext.h"
#include "pspguwrapper.h"

#include <pspkernel.h>
#include <pspgu.h>
#include <pspgum.h>

ErrorState::ErrorState(ConfigurationMenu *ctx, const std::string& msg)
    : State(ctx)
    , m_msg(msg)
    , m_text(new SimpleText())
{

}

ErrorState::~ErrorState(void)
{
    delete m_text;
}

void ErrorState::begin(StatePtr caller)
{
    init();
}

void ErrorState::end(void)
{

}

Scene *ErrorState::scene(void)
{
    return this;
}

void ErrorState::init(void)
{
    // set clear colour to black
    sceGuClearColor(0xff000000);
    sceGuClearDepth(0);
}

void ErrorState::render(void)
{
    sceGuStart(GU_DIRECT, PSPGUManager::instance()->displayList());
    sceGuClear(GU_COLOR_BUFFER_BIT|GU_DEPTH_BUFFER_BIT);

    // setup ortho projection to 2 length cube
    sceGumMatrixMode(GU_PROJECTION);
    sceGumLoadIdentity();

    // set view and model to identity
    sceGumMatrixMode(GU_VIEW);
    sceGumLoadIdentity();

    sceGumMatrixMode(GU_MODEL);
    sceGumLoadIdentity();

    sceKernelDcacheWritebackAll();
    m_text->drawCentered(480.f/2.f, 272.f/2.f, m_msg.c_str());

    m_text->render();

    sceGuFinish();
    sceGuSync(0,0);
    sceGuSwapBuffers();
}

void ErrorState::update(float dt)
{

}
