/*

Copyright (C) 2016, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

*/

#include "installupdate.h"
#include "mutexlocker.h"
#include "updateinformationparser.h"
#include "updateinformation.h"
#include "psardrv.h"

#include <fstream>
#include <vector>
#include <algorithm>

#include <pspiofilemgr.h>
#include <pspthreadman.h>
#include <pspmodulemgr.h>
#include <psputils.h>

#include <libinfinity.h>
#include <bootloaderupdater.h>

#include <unistd.h>

namespace kbridge
{
    #include <infinity_kinstaller.h>
};

#include <string.h>

namespace
{
    const unsigned char g_631_update_sha1[2][0x14] =
    {
        // normal updater
        {
            0x5C, 0x61, 0x6D, 0xCF, 0x04, 0x9D, 0x07, 0x07, 0xC0, 0xC8,
            0x3E, 0xA3, 0x91, 0x10, 0xFE, 0x4F, 0x38, 0x4E, 0x72, 0x2D
        },

        // go updater
        {
            0x78, 0x9D, 0x41, 0xC1, 0xAC, 0xE3, 0x46, 0x3C, 0x86, 0x25,
            0x83, 0x66, 0x49, 0x07, 0x54, 0x19, 0xB3, 0x02, 0x79, 0xC6
        }
    };

    int isPspGo(void)
    {
        return kbridge::getTrueModel() == 4 ? (1) : (0);
    }

    int VerifyFileSHA1(const char *filename, const unsigned char *check, u8 *work_buffer, int size)
    {
    	int read;
    	u8 sha1[20];
    	SceKernelUtilsSha1Context ctx;

    	SceUID fd = sceIoOpen(filename, PSP_O_RDONLY, 0);

    	if (fd < 0)
    	{
            return -1;
    	}

    	sceKernelUtilsSha1BlockInit(&ctx);

    	while ((read = sceIoRead(fd, work_buffer, size)) > 0)
    	{
    		sceKernelUtilsSha1BlockUpdate(&ctx, work_buffer, read);
    	}

    	sceKernelUtilsSha1BlockResult(&ctx, sha1);
    	sceIoClose(fd);

    	if (memcmp(sha1, check, 20) != 0)
    	{
            return -2;
    	}

        return 0;
    }

    // GCC 4.9 is missing std::align
        inline void *align( std::size_t alignment, std::size_t size,
                        void *&ptr, std::size_t &space ) {
    	std::uintptr_t pn = reinterpret_cast< std::uintptr_t >( ptr );
    	std::uintptr_t aligned = ( pn + alignment - 1 ) & - alignment;
    	std::size_t padding = aligned - pn;
    	if ( space < size + padding ) return nullptr;
    	space -= padding;
    	return ptr = reinterpret_cast< void * >( aligned );
    }

    static int flash_files(const char *section, char *buffer, int bufferSize)
    {
    	int size, cur, total;
    	char file[256];
        int result = 0;

        if (kbridge::mfcOpenSection((char *)section) < 0)
    	{
            return -1;
    	}

    	while (kbridge::mfcGetNextFile(file, &size,  &cur, &total, buffer, bufferSize) >= 0)
    	{
            std::ofstream flash0(file, std::ofstream::binary | std::ofstream::trunc);

            if (!flash0.is_open())
            {
                result = -2;
                continue;
            }

            flash0.write(buffer, size);
            flash0.close();
    	}

    	if (kbridge::mfcCloseSection() < 0)
    	{
            return -3;
    	}

        return result;
    }
};

InstallUpdate::InstallUpdate(void)
    : m_state(WAITING_FOR_UPDATE)
{
    m_eventId = sceKernelCreateEventFlag("infinityInstall", 0, 0, NULL);
}

InstallUpdate::~InstallUpdate(void)
{
    sceKernelDeleteEventFlag(m_eventId);
    kbridge::mfcCloseSection();
    kbridge::mfcClose();
}

InstallUpdate::State InstallUpdate::state(void) const
{
    return m_state;
}

void InstallUpdate::cancel(void)
{
    MutexLocker locker(&m_mutex);
    sceKernelSetEventFlag(m_eventId, EVENT_CANCEL);
}

void InstallUpdate::install(void)
{
    MutexLocker locker(&m_mutex);
    sceKernelSetEventFlag(m_eventId, EVENT_INSTALL);
}

void InstallUpdate::setState(State state)
{
    MutexLocker locker(&m_mutex);
    m_state = state;
}

void InstallUpdate::run(void)
{
    // swap directory
    char cwd[256];
    sceIoChdir(getcwd(cwd, sizeof(cwd)));

    // read file into memory
    std::vector<char> update;
    std::ifstream updateFile("UPDATE.MFC", std::ifstream::binary);

    if (!updateFile.is_open())
    {
        setState(UPDATE_UNAVAILABLE);
        return;
    }

    auto fsize = updateFile.tellg();
    updateFile.seekg(0, std::ios::end);
    fsize = updateFile.tellg() - fsize;
    updateFile.seekg(0, std::ios::beg);

    update.reserve(fsize);
    update.assign(std::istreambuf_iterator<char>(updateFile), std::istreambuf_iterator<char>());

    auto isOfficial = false;

    // now verify the signature
    switch (kbridge::verifyUpdate(update.data(), update.size()))
    {
    case kbridge::OFFICIAL_UPDATE:
        isOfficial = true;
        break;
    case kbridge::UNOFFICIAL_UPDATE:
        isOfficial = false;
        break;
    case kbridge::INVALID_UPDATE:
        setState(INVALID_UPDATE);
        return;
    }

    {
        // write MFC file
        std::ofstream container("CONTENTS.MFC", std::ofstream::binary | std::ofstream::trunc);

        if (!container.is_open())
        {
            setState(CONTAINER_WRITE_FAILURE);
            return;
        }

        container.write(update.data()+0x38, update.size()-0x38);
    }

    // load mfc file
    auto res = kbridge::mfcOpen((char *)"CONTENTS.MFC");

    if (res < 0)
    {
        setState(CONTAINER_OPEN_FAILURE);
        return;
    }

    if (kbridge::mfcOpenSection((char *)"UPDATER_VERSION") < 0)
    {
        setState(NO_UPDATER_VERSION);
        return;
    }

    char filename[256];
    int size = 0;
    auto gotVersion = false;
    UpdateInformationPtr info;

    while (kbridge::mfcGetNextFile(filename, &size,  NULL, NULL, NULL, 0) >= 0)
    {
        std::string file = std::string(filename);

        if (file == "version")
        {
            char *data = new char [size+1];

            if (kbridge::mfcGetNextFile(filename, &size, NULL, NULL, data, size) < 0)
            {
                delete[] data;
                kbridge::mfcCloseSection();
                kbridge::mfcClose();
                setState(NO_INTERNAL_VERSION);
                return;
            }

            // add null terminator
            data[size] = '\0';

            // parse the update version
            info = parseUpdateInformation(data);
            delete[] data;

            if (info->coreVersion() <= infGetCoreVersion()
            && (info->subsetVersion() <= infGetVersion()))
            {
                setState(NO_UPDATE_REQUIRED);
                kbridge::mfcCloseSection();
                kbridge::mfcClose();
                return;
            }

            if (info->coreVersion() > infGetCoreVersion())
            {
                // check 6.31 PBP
                setState(CHECKING_631_OFW);
                auto buffer = new u8 [1*1024*1024];

                if (VerifyFileSHA1("631.PBP", g_631_update_sha1[isPspGo()], buffer, 1*1024*1024) != 0)
                {
                    delete[] buffer;
                    setState(MISSING_631_OFW);
                    kbridge::mfcCloseSection();
                    kbridge::mfcClose();
                    return;
                }

                delete[] buffer;
            }

            gotVersion = true;
        }
    }

    kbridge::mfcCloseSection();

    setState(isOfficial ? (OFFICIAL_UPDATE_AVAILABLE) : (UNOFFICIAL_UPDATE_AVAILABLE));

    // wait for user to decide which event to pursue
    unsigned int events = 0;
    sceKernelWaitEventFlag(m_eventId, PSP_EVENT_WAITOR | PSP_EVENT_WAITCLEAR, EVENT_CANCEL | EVENT_INSTALL, &events, NULL);

    if (events & EVENT_CANCEL)
    {
        kbridge::mfcClose();
        return;
    }

    // do install
    setState(INSTALLING);

    // load modules in the UTILITY section
    if (kbridge::mfcOpenSection((char *)"UTILITY") < 0)
    {
        kbridge::mfcClose();
        setState(NO_UTILITIES);
        return;
    }

    std::vector<SceUID> modules;

    while (kbridge::mfcGetNextFile(filename, &size,  NULL, NULL, NULL, 0) >= 0)
    {
        u32 bufsize = size+64;
        char *data = new char [size+64];
        void *datap = data;
        char *module = reinterpret_cast<char *>(align(64, size, datap, bufsize));

        if (kbridge::mfcGetNextFile(filename, &size, NULL, NULL, module, size) < 0)
        {
            delete[] data;
            kbridge::mfcCloseSection();
            kbridge::mfcClose();
            setState(NO_UTILITIES);
            return;
        }

        auto modid = kbridge::mfcKernelLoadModuleBuffer((void *)size, (SceSize)module, 0, NULL);
        delete[] data;

        if (modid < 0)
        {
            kbridge::mfcCloseSection();
            kbridge::mfcClose();

            std::for_each(modules.begin(), modules.end(), [](auto modid)
            {
                sceKernelStopModule(modid, 0, NULL, NULL, NULL);
                sceKernelUnloadModule(modid);
            });

            setState(CANNOT_LOAD_UTILITY);
            return;
        }

        if (sceKernelStartModule(modid, 0, NULL, NULL, NULL) < 0)
        {
            kbridge::mfcCloseSection();
            kbridge::mfcClose();

            std::for_each(modules.begin(), modules.end(), [](auto modid)
            {
                sceKernelStopModule(modid, 0, NULL, NULL, NULL);
                sceKernelUnloadModule(modid);
            });

            setState(CANNOT_START_UTILITY);
            return;
        }

        modules.push_back(modid);
    }

    kbridge::mfcCloseSection();

    if (sceIoUnassign("flash0:") < 0)
    {
        kbridge::mfcClose();
        setState(ERROR_UNASSIGN_FLASH0);
        return;
    }

    if (sceIoAssign("flash0:", "lflash0:0,0", "flashfat0:", IOASSIGN_RDWR, NULL, 0) < 0)
    {
        kbridge::mfcClose();
        setState(ERROR_ASSIGN_FLASH0);
        return;
    }

    infSetRedirectionStatus(0);

    // allocate work buffer
    unsigned int workbufferSize = 1*1024*1024 + 64;
    char *workbuffer = new char [workbufferSize];
    void *workbufferp = workbuffer;
    align(64, 1*1024*1024, workbufferp, workbufferSize);

    // do we need to perform a core update?
    if (info->coreVersion() > infGetCoreVersion())
    {
        setState(FLASHING_BOOTLOADER);

        if (PSARDrive::instance()->mount("631.PBP") < 0)
        {
            infSetRedirectionStatus(1);
            delete[] workbuffer;
            setState(ERROR_MOUNTING_631);
            kbridge::mfcClose();
            return;
        }

        std::fstream systimer("psar0:/flash0/kd/systimer.prx");

        if (!systimer.is_open())
        {
            delete[] workbuffer;
            setState(ERROR_READING_SYSTIMER_PRX);
            infSetRedirectionStatus(1);
            PSARDrive::instance()->unmount();
            kbridge::mfcClose();
            return;
        }

        auto systimerStart = systimer.tellg();
        systimer.seekg(0, std::ios::end);
        unsigned int systimerSize = systimer.tellg() - systimerStart;
        systimer.seekg(0, std::ios::beg);
        unsigned int systimerBufferSize = systimerSize+64;

        char *systimerData = new char [systimerBufferSize];
        void *systimerDatap = systimerData;
        align(64, systimerSize, systimerDatap, systimerBufferSize);

        systimer.read(reinterpret_cast<char *>(systimerDatap), systimerSize);
        systimer.close();

        std::fstream init("psar0:/flash0/kd/init.prx");

        if (!init.is_open())
        {
            setState(ERROR_READING_INIT_PRX);
            infSetRedirectionStatus(1);
            delete[] workbuffer;
            delete[] systimerData;
            PSARDrive::instance()->unmount();
            kbridge::mfcClose();
            return;
        }

        auto initStart = init.tellg();
        init.seekg(0, std::ios::end);
        unsigned int initSize = init.tellg() - initStart;
        init.seekg(0, std::ios::beg);
        unsigned int initBufferSize = initSize+64;

        char *initData = new char [initBufferSize];
        void *initDatap = initData;
        align(64, initSize, initDatap, initBufferSize);

        init.read(reinterpret_cast<char *>(initDatap), initSize);
        init.close();

        if (infBootloaderUpdaterFlashBootloader(workbufferp, 1*1024*1024, systimerDatap, systimerSize, initDatap, initSize) < 0)
        {
            setState(ERROR_FLASHING_BOOTLOADER);
            infSetRedirectionStatus(1);
            delete[] workbuffer;
            delete[] systimerData;
            delete[] initData;
            PSARDrive::instance()->unmount();
            kbridge::mfcClose();
        }

        delete[] systimerData;
        delete[] initData;

        if (PSARDrive::instance()->unmount() < 0)
        {
            setState(ERROR_UNMOUNTING_631);
            infSetRedirectionStatus(1);
            delete[] workbuffer;
            kbridge::mfcClose();
            return;
        }

    	sceKernelDelayThread(1200000);

        // flash CORE flash0 files
        setState(FLASHING_CORE_FLASH0);

        switch (flash_files("CORE", workbuffer, workbufferSize))
        {
        case -1:
            setState(ERROR_OPENING_CORE_FLASH0);
            infSetRedirectionStatus(1);
            delete[] workbuffer;
            kbridge::mfcClose();
            return;
        case -2:
            setState(ERROR_FLASHING_CORE_FLASH0);
            infSetRedirectionStatus(1);
            delete[] workbuffer;
            kbridge::mfcClose();
            return;
        case -3:
            setState(ERROR_CLOSING_CORE_FLASH0);
            infSetRedirectionStatus(1);
            delete[] workbuffer;
            kbridge::mfcClose();
            return;
        }

    	sceKernelDelayThread(1200000);
    }

    if (info->subsetVersion() > infGetVersion())
    {
        // flash subset flash0 files
        setState(FLASHING_SUBSET_FLASH0);

        switch (flash_files("661", workbuffer, workbufferSize))
        {
        case -1:
            setState(ERROR_OPENING_SUBSET_FLASH0);
            infSetRedirectionStatus(1);
            delete[] workbuffer;
            kbridge::mfcClose();
            return;
        case -2:
            setState(ERROR_FLASHING_SUBSET_FLASH0);
            infSetRedirectionStatus(1);
            delete[] workbuffer;
            kbridge::mfcClose();
            return;
        case -3:
            setState(ERROR_CLOSING_SUBSET_FLASH0);
            infSetRedirectionStatus(1);
            delete[] workbuffer;
            kbridge::mfcClose();
            return;
        }

    	sceKernelDelayThread(1200000);
    }

    setState(INSTALL_COMPLETE);
    infSetRedirectionStatus(1);
    delete[] workbuffer;
    kbridge::mfcClose();
}
