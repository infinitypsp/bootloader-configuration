/*

Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.


 */

#include "blockallocator.h"

// hack to allow compilation
// these features aren't used in the portion of boost we're interested in
// but boost does not have the appropriate abstractions
#define BOOST_HAS_UNISTD_H
#define CLOCK_MONOTONIC_PRECISE
#define BOOST_INTERPROCESS_POSIX_TIMES_WRK_HPP

#include <boost/interprocess/mem_algo/rbtree_best_fit.hpp>
#include <boost/interprocess/sync/null_mutex.hpp>

namespace bi = boost::interprocess;

namespace {
struct null_mutex_family
{
    typedef bi::null_mutex mutex_type;
    typedef bi::null_mutex recursive_mutex_type;
};
}

using BoostAllocator = bi::rbtree_best_fit<null_mutex_family, bi::offset_ptr<void> >;

class Allocator : public BoostAllocator
{
public:
    Allocator(unsigned int size)
        : BoostAllocator(size, sizeof(BoostAllocator)) { }
};

BlockAllocator::BlockAllocator(void *base, unsigned int size)
{
    m_allocator = new(base) Allocator(size);
}

void *BlockAllocator::allocate(unsigned int size)
{
    return m_allocator->allocate(size);
}

void BlockAllocator::deallocate(void *ptr)
{
    return m_allocator->deallocate(ptr);
}

void *BlockAllocator::allocate_aligned(unsigned int size, unsigned int alignment)
{
    return m_allocator->allocate_aligned(size, alignment);
}

#undef BOOST_HAS_UNISTD_H
#undef CLOCK_MONOTONIC_PRECISE
#undef BOOST_INTERPROCESS_POSIX_TIMES_WRK_HPP
