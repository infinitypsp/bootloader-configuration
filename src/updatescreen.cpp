/*

Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

*/

#include "updatescreen.h"
#include "textrenderer.h"
#include "font2.h"
#include "application.h"
#include "buttonevent.h"
#include "colouredrectangle.h"
#include "menuselectiontransition.h"
#include "internetupdatescreen.h"
#include "installupdatescreen.h"
#include "animations.h"

#include <libinfinity.h>
#include <infinity_kinstaller.h>

#include <iostream>
#include <sstream>
#include <cmath>

#include <pspgu.h>
#include <pspiofilemgr.h>
#include <pspctrl.h>

#include "vertextype.h"
#include <pspgum.h>

#include "numberanimation.h"

UpdateScreen::UpdateScreen(ViewManager *viewManager)
    : PageView(viewManager, "Update Infinity")
    , m_latinText(new TextRenderer(new Font("flash0:/font/ltn0.pgf")))
    , m_opacity(1.0f)
    , m_t(0.f)
    , m_selectedItem(0)
    , m_menu(this)
    , m_installScreen(new InstallUpdateScreen(viewManager, this))
    , m_internetScreen(new InternetUpdateScreen(viewManager, this, m_installScreen.get()))
{
    m_menu.setX(36.f);
    m_menu.setY(100.f);

    //auto localTransition = new MenuSelectionTransition(&m_menu, "Update from Memory Card/Internal Storage");
    //auto internetTransition = new MenuSelectionTransition(&m_menu, "Update from Internet");

    //smoothFadeOutAnimation(this, m_installScreen.get(), localTransition);
    //smoothFadeOutAnimation(this, m_internetScreen.get(), internetTransition);

    std::stringstream ver1;
    ver1 << "Infinity Version: R" << infGetVersion() << std::endl;
    ver1 << "Infinity Core Version: R" << infGetCoreVersion();

    m_version = ver1.str();
}

void UpdateScreen::setOpacity(float opacity)
{
    m_opacity = opacity;
    m_latinText->font()->setOpacity(m_opacity);
}

float UpdateScreen::opacity(void)
{
    return m_opacity;
}

void UpdateScreen::update(float dt)
{
    m_menu.update(dt);
    PageView::update(dt);
}

void UpdateScreen::render(void)
{
    m_latinText->drawVerticalTop(480.f/2.f, 50.f, m_version, TextRenderer::ALIGN_CENTER);
    m_latinText->font()->setColour(0xFF00FFFF);
    m_latinText->draw(480.f/2.f, 272.f/2.f, "Unavailable due to unforeseen consequences...", TextRenderer::ALIGN_CENTER);
    m_latinText->font()->setColour(0xFFFFFFFF);
    //m_menu.draw();
    PageView::render();
}
