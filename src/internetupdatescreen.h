/*

Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

*/

#ifndef INTERNETUPDATESCREEN_H
#define INTERNETUPDATESCREEN_H

#include "pageview.h"

#include <string>
#include <memory>
#include <functional>

#include <pspkerneltypes.h>

class TextRenderer;
class InternetUpdateWorker;
class Event;
class UpdateInformation;

using UpdateInformationPtr = std::shared_ptr<UpdateInformation>;

class InternetUpdateScreen : public PageView
{
public:
    InternetUpdateScreen(ViewManager *viewManager, PageView *parent, PageView *installScreen);

    void setOpacity(float opacity) override;
    float opacity(void) override;

protected:
    void update(float dt) override;
    void render(void) override;

    void begin(State *parent) override;

private:
    enum InternalState
    {
        DISCONNECTED,
        GOT_UPDATE_INFO,
        DOWNLOADING_UPDATE,
        ON_LATEST,
        GOT_UPDATE,
        CONNECTION_FAILED,
        UPDATING
    };

    void onEvent(Event *event);

private:
    float m_opacity;
    bool m_netInitialised;
    TextRenderer *m_latinText;
    std::string m_status;
    InternetUpdateWorker *m_worker;
    SceUID m_mutex;
    InternetUpdateScreen::InternalState m_state;
    UpdateInformationPtr m_updateInfo;
    PageView *m_parentView, *m_installScreen;
};

#endif // INTERNETUPDATESCREEN_H
