/*

Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

*/

#include "homescreen.h"
#include "textrenderer.h"
#include "font2.h"
#include "fontmanager.h"
#include "viewmanager.h"
#include "graphicsdevice.h"
#include "blockallocator.h"
#include "particlesource.h"
#include "vertextype.h"

#include <pspgu.h>
#include <pspgum.h>

#define BOOST_NO_RTTI
#define BOOST_NO_EXCEPTIONS

#include <boost/circular_buffer.hpp>

#include "texture.h"

void boost::throw_exception(std::exception const & e)
{
    // do nothing
}

class TailedParticle
{
public:
    TailedParticle(GraphicsDevice *device, int length)
        : m_graphicsDevice(device)
        , m_particleN(length)
        , m_particleSize(10.f)
        , m_particleSource(new ParticleSource(0, 0, 750))
        , m_opacity(1.f)
    {
        m_particleSource->setRepeat(true);

        m_verticesVRAM = (TextureVertex *)m_graphicsDevice->vramAllocator()->allocate_aligned(2*m_particleN*sizeof(TextureVertex), 16);

        // set default parameters for the vertex
        TextureVertex vertex;

        vertex.colour = 0;
        vertex.u = vertex.v = 0.0f;
        vertex.x = vertex.y = -1.f;
        vertex.z = 0.f;

        //m_vertices.resize(m_particleN, vertex);
        m_vertices2.resize(2*m_particleN, vertex);

        int is_second = 0;
        for (int i = 0; i < m_particleN; ++i)
        {
            m_vertices2[(2*i)+1].u = m_vertices2[(2*i)+1].v = 127.f;
        }

        // load texture
        m_texture = (unsigned char *)m_graphicsDevice->vramAllocator()->allocate_aligned(128*128*4, 16);

        for (int i = 0; i < 128*128; ++i)
        {
            unsigned char color_cap = 0;

            if (g_infinityTex[i] > 0x90 && g_infinityTex[i] < 0xB0)
            {
                color_cap = 0x90;
            }

            m_texture[4*i] = m_texture[4*i+1] = m_texture[4*i+2] = g_infinityTex[i] - color_cap;
            m_texture[4*i+3] = g_infinityTex[i];
        }
    }

    void moveTo(float x, float y)
    {
        TextureVertex vertex;

        vertex.colour = 0;
        vertex.u = vertex.v = 128.0f;
        vertex.x = x+m_particleSize/2.0f;
        vertex.y = y+m_particleSize/2.0f;
        vertex.z = 0.f;

        m_vertices2.push_front(vertex);

        vertex.u = vertex.v = 0.0f;
        vertex.x = x-m_particleSize/2.0f;
        vertex.y = y-m_particleSize/2.0f;
        vertex.z = 0.f;

        m_vertices2.push_front(vertex);

        // update colours and size
        for (int i = 0; i < m_particleN; ++i)
        {
            unsigned char alpha = (215.f * (float)(m_particleN - i)/(float)m_particleN + 40.f)*m_opacity;

            m_vertices2[2*i].colour = 0x00FFFFFF | ((int)(alpha*m_opacity) << 24);
            m_vertices2[(2*i)+1].colour = 0x00FFFFFF | ((int)(alpha*m_opacity) << 24);
        }

        float x2 = x;
        float y2 = y;

        float vx = x - m_vertices2[3].x;
        float vy = y - m_vertices2[3].y;

        // get direction
        if (vy != 0.f || vx != 0.f)
        {
            float theta = atan2f(vy, vx);
            x2 = x + (m_particleSize-2.f)/2.0f * cosf(theta);
            y2 = y + (m_particleSize-2.f)/2.0f * sinf(theta);
        }

        m_particleSource->setSpawnArea(Rectangle(x-m_particleSize/2.0f, y-m_particleSize/2.0f, m_particleSize, m_particleSize));
    }

    void update(float dt)
    {
        m_particleSource->update(dt);
    }

    void draw(void)
    {
        // copy vertices
        copyCircularBuffer(m_verticesVRAM, m_vertices2);

        sceGuTexMode(GU_PSM_8888, 0, 0, GU_FALSE);
        sceGuTexFunc(GU_TFX_ADD, GU_TCC_RGBA);
        sceGuTexImage(0, 128, 128, 128, m_texture);
        sceGuTexFilter(GU_NEAREST, GU_NEAREST);
        sceGuTexScale(1.0f/128.f, 1.0f/128.f);
        sceGuTexOffset(0.0f, 0.0f);
        sceGuAmbientColor(0xffffffff);

        sceKernelDcacheWritebackAll();
        sceGuTexFlush();
        sceGuTexSync();

        sceGuEnable(GU_TEXTURE_2D);
        sceGumDrawArray(GU_SPRITES, GU_COLOR_8888 | GU_TEXTURE_32BITF | GU_VERTEX_32BITF | GU_TRANSFORM_3D, 2*m_particleN, 0, m_verticesVRAM);
        sceGuDisable(GU_TEXTURE_2D);

        m_particleSource->render();
    }

    void setOpacity(float opacity)
    {
        m_opacity = opacity;
    }

private:
    template <class T>
    void copyCircularBuffer(void *dst, const boost::circular_buffer<T>& buffer)
    {
        memcpy(dst, buffer.array_one().first, buffer.array_one().second*sizeof(T));
        memcpy((char *)dst+buffer.array_one().second*sizeof(T), buffer.array_two().first, buffer.array_two().second*sizeof(T));
    }

private:
    GraphicsDevice *m_graphicsDevice;
    int m_particleN;
    float m_particleSize;
    std::vector<TextureVertex> m_vertices;
    TextureVertex *m_verticesVRAM;
    boost::circular_buffer<TextureVertex> m_vertices2;
    unsigned char *m_texture;
    ParticleSource *m_particleSource;
    float m_opacity;
};

HomeScreen::HomeScreen(ViewManager *viewManager)
    : PageView(viewManager, "Home")
    , m_latinText(new TextRenderer(FontManager::defaultFont()))
    , m_particles(new TailedParticle(viewManager->graphics(), 175))
    , m_opacity(1.0f)
{

}

void HomeScreen::setOpacity(float opacity)
{
    m_opacity = opacity;
    m_latinText->font()->setOpacity(m_opacity);
    m_particles->setOpacity(opacity);
}

float HomeScreen::opacity(void)
{
    return m_opacity;
}

void HomeScreen::update(float dt)
{
    m_t += dt;

    float t = m_t/1.75f;
    float scale = 2.0f / (3.0f - cosf(2.0f*t));

    float modelx = scale * cosf(t);
    float modely = scale * sinf(2.f*t) / 2.f;

    float width = 400.f;
    float height = 400.f;
    float xoffset = 480-width-5;
    float yoffset = 272.f/2.f - height/2.f;

    float x = width*((modelx+1)/2.f) + xoffset/2.f;
    float y = height*((modely+1)/2.f) + yoffset;

    m_particles->moveTo(x, y);
    m_particles->update(dt);

    PageView::update(dt);
}

void HomeScreen::render(void)
{
	sceGuDisable(GU_TEXTURE_2D);
    m_latinText->drawVerticalTop(480.f/2.f, 50.f, "Welcome to Infinity.", TextRenderer::ALIGN_CENTER);
    m_particles->draw();
    PageView::render();
}
