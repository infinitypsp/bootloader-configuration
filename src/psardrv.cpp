/*
Copyright (C) 2016, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

*/

#include "psardrv.h"

#include <pspiofilemgr.h>

#define PSAR_CTL_CMD_MOUNT          (1)
#define PSAR_CTL_CMD_UMOUNT         (2)

PSARDrive *PSARDrive::m_instance = nullptr;

PSARDrive *PSARDrive::instance(void)
{
    if (!m_instance)
        m_instance = new PSARDrive();

    return m_instance;
}

PSARDrive::PSARDrive(void)
{
    unmount();
}

int PSARDrive::mount(const std::string& filename)
{
    return sceIoDevctl("psar0:", PSAR_CTL_CMD_MOUNT, (u8 *)filename.c_str(), filename.length()+1, NULL, 0);
}

int PSARDrive::unmount(void)
{
    return sceIoDevctl("psar0:", PSAR_CTL_CMD_UMOUNT, NULL, 0, NULL, 0);
}
