/*

Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

*/

#include "internetupdatescreen.h"
#include "updateinformation.h"
#include "updateinformationparser.h"
#include "textrenderer.h"
#include "font2.h"
#include "application.h"
#include "buttonevent.h"
#include "installupdatescreen.h"

#include <libinfinity.h>

#include <pspuser.h>
#include <psputility.h>
#include <pspnet.h>
#include <pspnet_inet.h>
#include <pspnet_apctl.h>
#include <psphttp.h>
#include <pspssl.h>

#include <iostream>
#include <sstream>
#include <fstream>

#include <stdio.h>

namespace
{
    // Simple RAII wrapper
    class UtilityModule
    {
    public:
        UtilityModule(int module)
            : m_module(module)
        {
            m_status = sceUtilityLoadModule(module);
        }

        ~UtilityModule(void)
        {
            if (m_status >= 0)
                sceUtilityUnloadModule(m_module);
        }

        int status(void) const
        {
            return m_status;
        }

    private:
        int m_module, m_status;
    };

    class MutexLocker
    {
    public:
        MutexLocker(SceUID mutex)
            : m_mutex(mutex)
        {
            sceKernelWaitSema(m_mutex, 1, NULL);
        }

        ~MutexLocker(void)
        {
            sceKernelSignalSema(m_mutex, 1);
        }

    private:
        SceUID m_mutex;
    };
} // anonymous


// THIS CODE NEEDS HELP
class InternetUpdateWorker
{
public:
    using CompletionHandler = std::function<void(std::vector<char>)>;
    using ErrorHandler = std::function<void(const std::string&, int)>;

public:
    InternetUpdateWorker(void)
        : m_status(0)
        , m_completionHandler(nullptr)
        , m_errorHandler(nullptr)
    {
        m_mutex = sceKernelCreateSema("IUWMUTEX", 0, 1, 1, NULL);

        if (m_mutex < 0)
        {
            m_error = "Could not create mutex";
            setStatus(m_mutex);
        }
    }

    // not reentrant
    void startDownload(const std::string& url, CompletionHandler completionHandler = nullptr, ErrorHandler errorHandler = nullptr)
    {
        m_url = url;
        m_completionHandler = completionHandler;
        m_errorHandler = errorHandler;

        SceUID thid = sceKernelCreateThread("downloader", &InternetUpdateWorker::beginDownload, 0x12, 0x10000, 0, NULL);

        if (thid >= 0)
        {
            InternetUpdateWorker *storage[] = { this };
            sceKernelStartThread(thid, 4, storage);
        }
    }

    int status(void)
    {
        MutexLocker locker(m_mutex);
        return m_status;
    }

    void setStatus(int status)
    {
        MutexLocker locker(m_mutex);
        m_status = status;
    }

    std::string error(void)
    {
        MutexLocker locker(m_mutex);
        return m_error;
    }

    const std::vector<char>& data(void)
    {
        return m_data;
    }

private:
    int run(void)
    {
        // load net modules
        UtilityModule common(PSP_MODULE_NET_COMMON);

        if (common.status() < 0)
        {
            m_error = "Could not initialise PSP_MODULE_NET_COMMON";

            return common.status();
        }

        UtilityModule adhoc(PSP_MODULE_NET_ADHOC);

        if (adhoc.status() < 0)
        {
            m_error = "Could not initialise PSP_MODULE_NET_ADHOC";

            return adhoc.status();
        }

        UtilityModule inet(PSP_MODULE_NET_INET);

        if (inet.status() < 0)
        {
            m_error = "Could not initialise PSP_MODULE_NET_INET";

            return inet.status();
        }

        UtilityModule parseuri(PSP_MODULE_NET_PARSEURI);

        if (parseuri.status() < 0)
        {
            m_error = "Could not initialise PSP_MODULE_NET_PARSEURI";

            return parseuri.status();
        }

        UtilityModule parsehttp(PSP_MODULE_NET_PARSEHTTP);

        if (parsehttp.status() < 0)
        {
            m_error = "Could not initialise PSP_MODULE_NET_PARSEHTTP";

            return parsehttp.status();
        }

        UtilityModule http(PSP_MODULE_NET_HTTP);

        if (http.status() < 0)
        {
            m_error = "Could not initialise PSP_MODULE_NET_HTTP";

            return http.status();
        }

        UtilityModule ssl(PSP_MODULE_NET_SSL);

        if (ssl.status() < 0)
        {
            m_error = "Could not initialise PSP_MODULE_NET_SSL";

            return ssl.status();
        }

        // intialise net
        auto res = sceNetInit(0x20000, 42, 0, 42, 0);

		if (res < 0)
        {
            m_error = "Could not initialise net library";
                if (m_errorHandler)
                    m_errorHandler(m_error, res);

            return res;
        }

        res = sceNetInetInit();

        if (res < 0)
        {
            m_error = "Could not initialise inet";
                if (m_errorHandler)
                    m_errorHandler(m_error, res);

            sceNetTerm();
            return res;
        }

        res = sceNetApctlInit(21504, 48);

        if (res < 0)
        {
            m_error = "Could not initialise apctl";
                if (m_errorHandler)
                    m_errorHandler(m_error, res);

            sceNetInetTerm();
            sceNetTerm();
            return res;
        }

        res = sceSslInit(0x28000);

        if (res < 0)
        {
            m_error = "Could not initialise SSL";
                if (m_errorHandler)
                    m_errorHandler(m_error, res);

            sceNetApctlTerm();
            sceNetInetTerm();
            sceNetTerm();
            return res;

        }

        res = sceNetApctlConnect(1);

        if (res < 0)
        {
            m_error = "Could not connect to access point";
                if (m_errorHandler)
                    m_errorHandler(m_error, res);

            sceSslEnd();
            sceNetApctlTerm();
            sceNetInetTerm();
            sceNetTerm();
            return res;
        }

        while (1)
        {
            int state;
            res = sceNetApctlGetState(&state);

            if (res < 0)
            {
                m_error = "Error getting APCTL state";
            error_exit:
                if (m_errorHandler)
                    m_errorHandler(m_error, res);

                sceSslEnd();
                sceNetApctlDisconnect();
                sceNetApctlTerm();
                sceNetInetTerm();
                sceNetTerm();
                return res;
            }

            if (state == PSP_NET_APCTL_STATE_GOT_IP)
            {
                break;
            }

            sceKernelDelayThread(50*1000);
        }

        res = sceHttpInit(20000);

        if (res < 0)
        {
            m_error = "Error initialising HTTP";
            goto error_exit;
        }

        res = sceHttpsInit(0, 0, 0, 0);

        if (res < 0)
        {
            m_error = "Error initialising HTTPS";
            goto error_exit;
        }

        res = sceHttpsLoadDefaultCert(0, 0);

        if (res < 0)
        {
            m_error = "Error loading default certificates";
            goto error_exit;
        }

        char useragent[256];
        sprintf(useragent, "infinity/RC%i/RS%i libhttp/1.0.0", infGetVersion(), infGetCoreVersion());
        auto httpTemplate = sceHttpCreateTemplate(useragent, 1, 0);

        if (httpTemplate < 0)
        {
            res = httpTemplate;
            m_error = "Error creating HTTP template";
            goto error_exit;
        }

        // set timeouts
        sceHttpSetResolveTimeOut(httpTemplate, 3*1000*1000);
        sceHttpSetRecvTimeOut(httpTemplate, 60*1000*1000);
        sceHttpSetSendTimeOut(httpTemplate, 60*1000*1000);

        auto connection = sceHttpCreateConnectionWithURL(httpTemplate, m_url.c_str(), 0);

        if (connection < 0)
        {
            res = connection;
            m_error = "Error creating HTTP connection";
            goto error_exit;
        }

        auto request = sceHttpCreateRequestWithURL(connection, PSP_HTTP_METHOD_GET , (char *)m_url.c_str(), 0);

        if (request < 0)
        {
            res = request;
            m_error = "Error creating HTTP request";
            goto error_exit;
        }

        res = sceHttpSendRequest(request, NULL, 0);

        if (res < 0)
        {
            m_error = "Error sending HTTP request";
            goto error_exit;
        }

        int status = 0;
        res = sceHttpGetStatusCode(request, &status);

        if (res < 0)
        {
            m_error = "Error getting HTTP status";
            goto error_exit;
        }

        if (status != 200)
        {
            res = 0x80000000 | status;
            m_error = "Error: got wrong HTTP status.";
            goto error_exit;
        }

        SceULong64 contentLength;
        res = sceHttpGetContentLength(request, &contentLength);

        if (res < 0)
        {
            m_error = "Error getting the content length";
            goto error_exit;
        }

        m_data.resize(contentLength);

        res = sceHttpReadData(request, m_data.data(), m_data.size());

        if (res < 0)
        {
            m_error = "Error reading http data";
            goto error_exit;
        }

        if (m_completionHandler != nullptr)
            m_completionHandler(m_data);

        sceHttpsEnd();
        sceHttpEnd();
        sceSslEnd();
        sceNetApctlDisconnect();
        sceNetApctlTerm();
        sceNetInetTerm();
        sceNetTerm();
        return 1;
    }

    static int beginDownload(SceSize args, void *argp)
    {
        InternetUpdateWorker *worker = *(InternetUpdateWorker **)argp;
        auto res = worker->run();
        worker->setStatus(res);
        return 0;
    }

private:
    std::string m_url, m_error;
    SceUID m_mutex;
    int m_status;
    std::vector<char> m_data;
    CompletionHandler m_completionHandler;
    ErrorHandler m_errorHandler;
};

InternetUpdateScreen::InternetUpdateScreen(ViewManager *viewManager, PageView *parent, PageView *installScreen)
    : PageView(viewManager, "Internet Update")
    , m_opacity(1.0f)
    , m_netInitialised(false)
    , m_latinText(new TextRenderer(new Font("flash0:/font/ltn0.pgf")))
    , m_status("Initialising...")
    , m_worker(new InternetUpdateWorker)
    , m_mutex(-1)
    , m_state(DISCONNECTED)
    , m_parentView(parent)
    , m_installScreen(installScreen)
{
    Application::instance()->addEventHandler([=](Event *event)
    {
        if (visible())
        {
            onEvent(event);
        }
    });

    m_mutex = sceKernelCreateSema("IUSMUTEX", 0, 1, 1, NULL);

    if (m_mutex < 0)
    {
        // TODO: what do I do here?!
    }
}

#include "numberanimation.h"
#include "buttontransition.h"
#include <pspctrl.h>

namespace
{
    void addSmoothFadeOut(auto source, auto dest, auto transition)
    {
        auto animation = std::make_shared<NumberAnimation>(0, 255);

        animation->setHandler([source, dest](float number)
        {
            source->setOpacity((255.f-number)/255.f);
            dest->setOpacity(number/255.f);
        });

        transition->addAnimation(animation);
        source->addTransition(transition, dest);
    }
};

void InternetUpdateScreen::begin(State *parent)
{
    PageView::begin(parent);

    m_worker->startDownload("http://infinity.lolhax.org/update.json", [this](auto data)
    {
        m_updateInfo = parseUpdateInformation(std::string(data.begin(), data.end()));

        std::stringstream ss;
        ss << "Found update version: " << m_updateInfo->version();

        {
            MutexLocker locker(m_mutex);
            m_state = GOT_UPDATE_INFO;
            m_status = ss.str();
        }
    },
    [this](auto error, auto code)
    {
        std::stringstream ss;
        ss << error << " (0x" << std::hex << code << ")";

        {
            MutexLocker locker(m_mutex);
            m_state =  CONNECTION_FAILED;
            m_status = ss.str();
        }

        auto transition = new ButtonTransition(PSP_CTRL_CIRCLE);
        addSmoothFadeOut(this, m_parentView, transition);
    });
}

void InternetUpdateScreen::setOpacity(float opacity)
{
    m_opacity = opacity;
    m_latinText->font()->setOpacity(opacity);
}

float InternetUpdateScreen::opacity(void)
{
    return m_opacity;
}

void InternetUpdateScreen::update(float dt)
{
    PageView::update(dt);
}

void InternetUpdateScreen::render(void)
{
    m_latinText->drawVerticalTop(480.f/2.f, 50.f, m_status, TextRenderer::ALIGN_CENTER);

    switch (m_state)
    {
    default:
    case DISCONNECTED:
        m_latinText->drawVerticalTop(480.f/2.f, 80.f, "Please wait...", TextRenderer::ALIGN_CENTER);
        break;
    case GOT_UPDATE_INFO:
        m_latinText->drawVerticalTop(480.f/2.f, 80.f, "Press X to download the update.", TextRenderer::ALIGN_CENTER);
        break;
    case GOT_UPDATE:
        m_latinText->drawVerticalTop(480.f/2.f, 80.f, "Press X to install.", TextRenderer::ALIGN_CENTER);
        break;
    case DOWNLOADING_UPDATE:
        m_latinText->drawVerticalTop(480.f/2.f, 80.f, "Downloading update...", TextRenderer::ALIGN_CENTER);
        break;
    case ON_LATEST:
    case CONNECTION_FAILED:
        m_latinText->drawVerticalTop(480.f/2.f, 80.f, "Press O to return.", TextRenderer::ALIGN_CENTER);
        break;
    };

    PageView::render();
}

void InternetUpdateScreen::onEvent(Event *event)
{
    if (event->type() != Event::Button)
        return;

    ButtonEvent *buttonEvent = static_cast<ButtonEvent*>(event);

    if (m_state == GOT_UPDATE_INFO && buttonEvent->buttons() == PSP_CTRL_CROSS)
    {
        m_state = DOWNLOADING_UPDATE;

        // begin download
        m_worker->startDownload(m_updateInfo->url(), [this](auto data)
        {
            std::ofstream update("UPDATE.MFC", std::ofstream::binary|std::ofstream::trunc);

            if (!update.is_open())
            {
                // error
                {
                    MutexLocker locker(m_mutex);
                    m_state =  CONNECTION_FAILED;
                    m_status = "Error opening \"UPDATE.MFC\" for writing";
                }

                return;
            }

            update.write(data.data(), data.size());

            std::stringstream ss;
            ss << "Downloaded update. size: " << data.size();

            {
                MutexLocker locker(m_mutex);
                m_state = GOT_UPDATE;
                m_status = ss.str();
            }

            auto transition = new ButtonTransition(PSP_CTRL_CROSS);
            addSmoothFadeOut(this, m_installScreen, transition);
        },
        [this](auto error, auto code)
        {
            std::stringstream ss;
            ss << error << " (0x" << std::hex << code << ")";

            {
                MutexLocker locker(m_mutex);
                m_state = CONNECTION_FAILED;
                m_status = ss.str();
            }

            auto transition = new ButtonTransition(PSP_CTRL_CIRCLE);
            addSmoothFadeOut(this, m_parentView, transition);
        });
    }
}
