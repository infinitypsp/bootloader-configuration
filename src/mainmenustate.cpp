/*

Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.


 */

#include "mainmenustate.h"
#include "errorstate.h"
#include "configurationmenu.h"
#include "buttonarbiter.h"
#include "flashingrectangle.h"
#include "gltext.h"
#include "pspguwrapper.h"

#include <pspkernel.h>
#include <pspgu.h>
#include <pspgum.h>
#include <pspctrl.h>

MainMenuState::MainMenuState(ConfigurationMenu *ctx)
    : State(ctx)
    , m_text(new SimpleText())
    , m_curSelection(0)
    , m_selector(new FlashingRectangle(0.f, 0.f, 0.f, 8.f))
    , m_t(0)
{
    m_menu =
    {
        { "Menu1", [ctx]() { return std::make_shared<ErrorState>(ctx, "Menu1 Error"); } },
        { "Menu2", [ctx]() { return std::make_shared<ErrorState>(ctx, "Menu2 Error"); } },
        { "Menu3", [ctx]() { return std::make_shared<ErrorState>(ctx, "Menu3 Error"); } },
        { "Exit", [ctx]() { sceKernelExitGame(); return nullptr; } },
    };
}

MainMenuState::~MainMenuState(void)
{
    delete m_text;
}

void MainMenuState::begin(StatePtr caller)
{
    m_t = 0;

    // bind button handlers
    context()->buttonArbiter()->bind(PSP_CTRL_UP, [&](void)
    {
        m_curSelection--;

        if (m_curSelection < 0)
            m_curSelection = m_menu.size() - 1;
    });

    context()->buttonArbiter()->bind(PSP_CTRL_DOWN, [&](void)
    {
        m_curSelection++;

        if ((unsigned int)m_curSelection >= m_menu.size())
            m_curSelection = 0;
    });

    context()->buttonArbiter()->bind(PSP_CTRL_CROSS, [&](void)
    {
        context()->setState(m_menu.at(m_curSelection).second());
    });

    // initialise our graphics related tasks
    init();
}

void MainMenuState::end(void)
{

}

Scene *MainMenuState::scene(void)
{
    return this;
}

void MainMenuState::init(void)
{
    // set clear colour to black
    sceGuClearColor(0xff000000);
    sceGuClearDepth(0);
}

void MainMenuState::render(void)
{
    sceGuStart(GU_DIRECT, PSPGUManager::instance()->displayList());
    sceGuClear(GU_COLOR_BUFFER_BIT|GU_DEPTH_BUFFER_BIT);

    // setup ortho projection to 2 length cube
    sceGumMatrixMode(GU_PROJECTION);
    sceGumLoadIdentity();

    // set view and model to identity
    sceGumMatrixMode(GU_VIEW);
    sceGumLoadIdentity();

    sceGumMatrixMode(GU_MODEL);
    sceGumLoadIdentity();

    sceKernelDcacheWritebackAll();

    for (auto i = (unsigned int)0; i < m_menu.size(); ++i)
    {
        m_text->draw(0, i*8, m_menu.at(i).first);
    }

    m_selector->setX(0);
    m_selector->setY(m_curSelection*8.f);
    m_selector->setWidth(m_menu.at(m_curSelection).first.length()*8);
    m_selector->setHeight(8.f);

    m_selector->draw();
    m_text->render();

    sceGuFinish();
    sceGuSync(0,0);
    sceGuSwapBuffers();
}

void MainMenuState::update(float dt)
{
    // update our timestep
    m_t += dt;

    // update our selector animation
    m_selector->update(m_t);
}
