/*

Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.


 */

#ifndef ERRORSTATE_H
#define ERRORSTATE_H

#include "state.h"
#include "scene.h"

#include <config.h>

class SimpleText;

class ErrorState : public State, public Scene
{
public:
    ErrorState(ConfigurationMenu *ctx, const std::string& msg);
    ~ErrorState(void);

    void begin(StatePtr caller) override;
    void end(void) override;
    Scene *scene(void) override;

    void init(void) override;
    void render(void) override;
    void update(float dt) override;

private:
    std::string m_msg;
    SimpleText *m_text;
};

#endif // ERRORSTATE_H
